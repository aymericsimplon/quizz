interface Question {
    question: string;
    reponses: {
        a: string;
        b: string;
        c: string;
        d: string;
    };
    correctAnswer: string;
}

interface CustomButton extends HTMLButtonElement {
    correctAnswer?: string;
}

const mesQuestions: Question[] = [
    {
        question: "Quel est le nom du protagoniste dans Hunter x Hunter",
        reponses: {
            a: "Gon",
            b: "Pomme",
            c: "Luffy",
            d: "Hisoka"
        },
        correctAnswer: "a"
    },
    {
        question: "Quel est le pouvoir de Luffy dans One Piece",
        reponses: {
            a: "Invisible",
            b: "Elastique",
            c: "Hypervitesse",
            d: 'La foudre'
        },
        correctAnswer: "b"
    },
    {
        question: "Quel est le nom du rival de San goku",
        reponses: {
            a: 'San gotrunks',
            b: "Naruto",
            c: 'Vegeta',
            d: 'Tortue génial'
        },
        correctAnswer: 'c'
    },
    {
        question: "Dans Bleach, quel est le nom des divisions gérés par des capitaines",
        reponses: {
            a: 'Les arrancars',
            b: 'Les espadas',
            c: 'Les hollow',
            d: 'Les shinigami'
        },
        correctAnswer: 'd'
    },
    {
        question: "Qui est le sabreur naviguant avec Luffy",
        reponses: {
            a: "Jotaro",
            b: "Sanji",
            c: "Zoro",
            d: "Gazo"
        },
        correctAnswer: "c"
    },
    {
        question: "Quel est le pouvoir de Natsu dans fairy tail",
        reponses: {
            a: "Glacer",
            b: "Le poison",
            c: "Le feu",
            d: 'La foudre'
        },
        correctAnswer: "c"
    },
    {
        question: "Qui est le protagoniste dans l'attaque des titans",
        reponses: {
            a: 'Livai Ackerman',
            b: "Lacrim Haruno",
            c: 'Eren Jaeger',
            d: 'Armin le blond'
        },
        correctAnswer: 'c'
    },
    {
        question: "Quel est le nom de l'héroine dans Sailor Moon",
        reponses: {
            a: 'Tsukino Usagi',
            b: 'Sakura Haruno',
            c: 'Maria Carré',
            d: 'Artoria'
        },
        correctAnswer: 'a'
    },
];

const questionElement = document.getElementById("question");
const boutonsReponse = document.getElementById("boutons-reponse");
const suivant = document.getElementById("suivant");

let questionIndex = 0;
let score = 0;

function startQuiz() {
    questionIndex = 0;
    score = 0;
    if (suivant) {
        suivant.innerHTML = "Next";
    }
    showQuestion();
}

function showQuestion(): void {

    if (boutonsReponse) {
        boutonsReponse.innerHTML = "";
    }

    if (questionIndex < mesQuestions.length) {
        let currentQuestion = mesQuestions[questionIndex];
        let questionNo = questionIndex + 1;
        if (questionElement) {
            questionElement.innerHTML = `${questionNo}. ${currentQuestion.question}`;
        }

        const keys: ("a" | "b" | "c" | "d")[] = ['a', 'b', 'c', 'd'];

        for (const key of keys) {
            if (boutonsReponse) {
                const button: CustomButton = document.createElement("button");
                button.innerHTML = currentQuestion.reponses[key];
                button.classList.add("btn");
                boutonsReponse.appendChild(button);

                button.correctAnswer = key;

                button.addEventListener("click", selectAnswer);
            }
        }
    } else {
        console.error("Toutes les questions ont été posées.");
    }
}

function selectAnswer(e: MouseEvent): void {
    const selectedBtn = e.target as CustomButton;
    const isCorrect = selectedBtn.correctAnswer === mesQuestions[questionIndex].correctAnswer;

    if (isCorrect) {
        selectedBtn.classList.add("correct");
        score++;
    } else {
        selectedBtn.classList.add("incorrect");
    }

    disableButtons();
    if (suivant) {
        suivant.style.display = "block";
    }
}

function disableButtons(): void {
    if (boutonsReponse) {
        Array.from(boutonsReponse.children).forEach(button => {
            if (isCustomButton(button)) {
                button.disabled = true;
                if (button.correctAnswer === mesQuestions[questionIndex].correctAnswer) {
                    button.classList.add("correct");
                } else {
                    button.classList.add("incorrect");
                }
            }
        });
    }
}

function isCustomButton(element: Element): element is CustomButton {
    return element.tagName.toLowerCase() === "button" && "correctAnswer" in element;
}

if (suivant) {
    suivant.addEventListener("click", () => {
        if (questionIndex < mesQuestions.length) {
            handleSuivant();
        } else {
            startQuiz();
        }
    });
}

function handleSuivant() {
    questionIndex++;
    if (questionIndex < mesQuestions.length) {
        showQuestion();
    } else {
        showScore();
    }
}

function showScore() {
    resetState();
    if (questionElement) {
        questionElement.innerHTML = `Ton score est ${score} sur ${mesQuestions.length}!`;
    }
    if (suivant) {
        suivant.innerHTML = "Recommence";
        suivant.style.display = "block";
    }
}

function resetState() {
    if (boutonsReponse) {
        boutonsReponse.innerHTML = "";
    }
    if (suivant) {
        suivant.style.display = "none";
    }
}

startQuiz();
